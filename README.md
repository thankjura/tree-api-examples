**Example plugin which uses Tree Customfield API**

Plugin page: https://marketplace.atlassian.com/apps/1219011/tree-customfield

**Download plugin from:**

https://marketplace.atlassian.com/apps/1219011/tree-customfield/version-history


**Add plugin to maven local repository:**


$ mvn install:install-file -Dfile=/path/to/tree-customfield-2.4.2.jar -DgroupId=ru.slie.jira -DartifactId=tree-customfield -Dversion=2.4.2 -Dpackaging=jar -DgeneratePom=true


**then add dep to pom.xml of your plugin:**
***
```xml
<dependency>
  <groupId>ru.slie.jira</groupId>
  <artifactId>tree-customfield</artifactId>
  <version>2.4.2</version>
  <scope>provided</scope>
</dependency>
```
***

**see examples in ru.slie.jira.tree_examples.MainServlet**