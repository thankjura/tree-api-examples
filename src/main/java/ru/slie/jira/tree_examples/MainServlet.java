package ru.slie.jira.tree_examples;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import ru.slie.jira.tree.manager.TreeSchemaManager;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainServlet extends HttpServlet {
    private final TemplateRenderer renderer;
    private final JiraBaseUrls jiraBaseUrls;
    private final WebSudoManager webSudoManager;
    private final UserManager userManager;
    private final CustomFieldManager customFieldManager;
    private final TreeSchemaManager treeSchemaManager;

    private String fieldId;

    @Autowired
    public MainServlet(@ComponentImport final TemplateRenderer renderer,
                       @ComponentImport final JiraBaseUrls jiraBaseUrls,
                       @ComponentImport final WebSudoManager webSudoManager,
                       @ComponentImport final UserManager userManager,
                       @ComponentImport final CustomFieldManager customFieldManager,
                       @ComponentImport final TreeSchemaManager treeSchemaManager) {
        this.renderer = renderer;
        this.jiraBaseUrls = jiraBaseUrls;
        this.webSudoManager = webSudoManager;
        this.userManager = userManager;
        this.customFieldManager = customFieldManager;
        this.treeSchemaManager = treeSchemaManager;
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            webSudoManager.willExecuteWebSudoRequest(request);
            // If websudo disabled
            UserProfile user = userManager.getRemoteUser(request);
            if (user == null || !userManager.isSystemAdmin(user.getUserKey())) {
                throw new WebSudoSessionException();
            }

            Map<String, Object> params = new HashMap<>();
            params.put("baseUrl", jiraBaseUrls.baseUrl());


            List<CustomField> customFields = new ArrayList<>();

            for (CustomField field: customFieldManager.getCustomFieldObjects()) {
                if (field.getCustomFieldType().getKey().startsWith("ru.slie.jira.tree-customfield")) {
                    customFields.add(field);
                }
            }

            if (fieldId != null) {
                CustomField field = customFieldManager.getCustomFieldObject(fieldId);
                if (field != null) {
                    params.put("options", treeSchemaManager.getRootOptionsForField(field));
                }
            }

            //MutableIssue issue;

            // Get customfieldValue
            // TreeOption option = (TreeOption) issue.getCustomFieldValue(customfieldManager.getCustomFieldObject("customfield_11000")) // For cf with single value
            // Collection<TreeOption> option = (Collection<TreeOption>) issue.getCustomFieldValue(customfieldManager.getCustomFieldObject("customfield_12000")) // For cf with multi value

            // Get TreeOption by id:
            //TreeOption treeOption1 = treeSchemaManager.getOptionById(10);
            // Get TreeOption by name:
            //FieldConfigScheme fieldConfigScheme = customFieldManager.getCustomFieldObject("customfield_11000").getRelevantConfig(issue);
            //TreeSchema treeSchema = treeSchemaManager.getSchema(fieldConfigScheme);
            //TreeOption treeOption2 = treeSchema.getOptionByName("MyOptionName");

            //issue.setCustomFieldValue(customFieldManager.getCustomFieldObject("customfield_11000"), treeOption1);




            params.put("fields", customFields);
            params.put("fieldId", fieldId);

            response.setContentType("text/html;charset=utf-8");
            renderer.render("/templates/page.vm", params, response.getWriter());
        } catch(WebSudoSessionException wes) {
            webSudoManager.enforceWebSudoProtection(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            webSudoManager.willExecuteWebSudoRequest(request);

            // If websudo disabled
            UserProfile user = userManager.getRemoteUser(request);
            if (user == null || !userManager.isSystemAdmin(user.getUserKey())) {
                throw new WebSudoSessionException();
            }

            fieldId = request.getParameter("fieldId");
            response.sendRedirect(request.getRequestURL().toString());

        } catch (WebSudoSessionException wes) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
